import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth/auth.guard';

@NgModule({
  exports: [ RouterModule ],
  imports: [
    RouterModule.forRoot([
      { path: 'login', loadChildren: 'src/app/features/login/login.module#LoginModule' },
      { 
        path: 'devices',
        loadChildren: 'src/app/features/devices/device.module#DeviceModule',
        canActivate: [ AuthGuard ]
      },
      { path: 'home', loadChildren: 'src/app/features/home/home.module#HomeModule' },
      { path: '**', redirectTo: 'home' },
    ]),
  ],
})
export class AppRoutingModule {}

