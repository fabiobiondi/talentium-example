import { Component } from '@angular/core';
import { ConfigService } from '../service/config.service';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'tal-navbar',
  template: `
    <nav class="navbar navbar-expand" 
    [ngClass]="{
      'navbar-dark bg-dark': config.theme === 'dark',
      'navbar-light bg-light': config.theme === 'light'
    }">
      <a class="navbar-brand" href="#">Navbar {{config.theme}}</a>

      <div class="navbar-collapse collapse">
        <ul class="navbar-nav">
            
          <li class="nav-item" ifLogged>
            <a class="nav-link" [routerLinkActive]="'active'" routerLink="home">Home</a>
          </li>
    
          <li class="nav-item">
            <a class="nav-link" routerLinkActive="active" routerLink="login">Login</a>
          </li>
          
          <li class="nav-item" ifLogged>
            <a class="nav-link" routerLinkActive="active" routerLink="devices">Devices</a>
          </li>
          
          <li class="nav-item" ifLogged>
            <a class="nav-link" (click)="logoutHandler()">Logout</a>
          </li>
          
        </ul>
      </div>
    </nav>
  `
})
export class NavbarComponent {

  constructor(
    public config: ConfigService,
    public authService: AuthService,
    private router: Router
  ) {

  }

  logoutHandler() {
    this.authService.logout();
    this.router.navigateByUrl('login')
  }
}
