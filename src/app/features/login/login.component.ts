import { Component } from '@angular/core';
import { Auth, AuthService } from '../../core/auth/auth.service';
import { Router } from '@angular/router';
import { debounceTime, distinctUntilChanged, filter } from 'rxjs/internal/operators';
import { LoginFormService } from './services/login-form.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'tal-login',
  template: `
    
    <div *ngIf="error"n>C' èun errore</div>
    
    
    <form [formGroup]="myForm">
      <input type="text" formControlName="username">  
      <input type="text" formControlName="password">
    </form>
    
    {{loginFormService.username}}
    <button (click)="login()">Login</button>
  `
})
export class LoginComponent {
  error: any;
  myForm: FormGroup;

  constructor(
    private authService: AuthService,
    private router: Router,
    public loginFormService: LoginFormService,
    public fb: FormBuilder
  ) {

    this.myForm = fb.group({
      username: ['guesttt', Validators.required],
      password: ['', Validators.required],
    })

    // const fromServer = { username: 'fabiob', password: 'cicciod' };
    const fromServer = window.localStorage.getItem('myform');
    this.myForm.setValue(JSON.parse(fromServer));

    this.myForm.valueChanges
      .pipe(
        // filter(text => text.length > 4 && text.length < 10),
        debounceTime(700),
        // distinctUntilChanged()
      )
      .subscribe(formValue => {
        console.log('save to', JSON.stringify(formValue));
        window.localStorage.setItem('myform', JSON.stringify(formValue));
      })


    this.authService.auth$
      .pipe(
        filter(res => res && res.isAuthenticated)
      )
      .subscribe((isAuth: Auth) => {
        this.router.navigateByUrl('home');
      });
  }

  login() {
    this.authService.login()
  }


  login2() {
    this.authService.login2()
      .subscribe((res) => {
        console.log(res)
        this.router.navigateByUrl('home')
      });

  }
}
