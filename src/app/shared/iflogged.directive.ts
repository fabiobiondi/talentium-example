import { Directive, HostBinding } from '@angular/core';
import { AuthService } from '../core/auth/auth.service';

@Directive({
  selector: '[ifLogged]'
})
export class IfloggedDirective {

  constructor(private authService: AuthService)  {}

  @HostBinding('style.display') get display() {
    const currentState = this.authService.auth$.getValue();
    return  currentState.isAuthenticated ?  null : 'none';
  }
}
