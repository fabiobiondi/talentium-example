import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'tal-card',
  template: `
    <div class="card mb-1">
      <div class="card-header"
           [ngClass]="getCustomCls()"
           *ngIf="title"
           (click)="toggle.emit()"
      >
        <i
          *ngIf="collapsable"
          class="fa"
          [ngClass]="{
             'fa-arrow-circle-down': opened,
             'fa-arrow-circle-right': !opened
          }"
        ></i>
        {{title}}
      </div>
      
      <div class="card-body"
           *ngIf="opened"
      >
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: []
})
export class CardComponent {
  @Input() title;
  @Input() alert;
  @Input() success;
  @Input() customCls;
  @Input() opened = true;
  @Input() collapsable = true;
  @Output() toggle = new EventEmitter();

  getCustomCls() {

    const cls = [];

    if (this.alert) cls.push('bg-danger')
    if (this.customCls) cls.push(this.customCls)

    return cls;
  }
}
