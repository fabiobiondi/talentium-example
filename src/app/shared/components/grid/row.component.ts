import { Component } from '@angular/core';

@Component({
  selector: 'tal-row',
  template: `
     
    <div class="row">
      <ng-content></ng-content>
    </div>
  `
})
export class RowComponent {

}


