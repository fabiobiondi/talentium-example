import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'tal-list',
  template: `
    <li class="list-group-item" 
      *ngFor="let item of items">
      {{item[labelField]}}
    </li>
  `,
  styles: []
})
export class ListComponent  {
  @Input() items;
  @Input() labelField = 'name';
}
