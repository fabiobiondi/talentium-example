import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Device } from '../model/device';

@Component({
  selector: 'tal-device-list',
  template: `
    <div class="container mt-3">
      <li
        class="list-group-item"
        [ngClass]="{'bg-info': device.id === active?.id}"
        *ngFor="let device of items"
        (click)="setActive.emit(device)"
      >
        <tal-os-icon [os]="device.os"></tal-os-icon>

        {{device.label}} 
        
        <tal-gmap 
          [lat]="device.lat" 
          [lng]="device.lng"
          size="70x30"
        ></tal-gmap>

        <div class="pull-right"
             [ngStyle]="{ color: device.price > 500 ? 'red' : null}"
        >
          € {{device.price}}

          <i class="fa fa-trash" 
             (click)="delete.emit(device)"></i>
        </div>
      </li>
    </div>
  `,
  styles: []
})
export class DeviceListComponent {
  // @Input() items: Device[];
  @Input() items: Array<Device>;
  @Input() active: Device;
  @Output() delete: EventEmitter<Device> = new EventEmitter<Device>();
  @Output() setActive: EventEmitter<Device> = new EventEmitter<Device>();
}
