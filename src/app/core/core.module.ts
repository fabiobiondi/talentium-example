import { NgModule } from '@angular/core';
import { NavbarComponent } from './components/navbar.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { IfloggedDirective } from '../shared/iflogged.directive';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ NavbarComponent ],
  exports: [ NavbarComponent ],
  imports: [
    SharedModule,
    CommonModule,
    RouterModule
  ],
  providers: []
})
export class CoreModule {}

