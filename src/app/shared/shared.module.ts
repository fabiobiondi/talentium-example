import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { COMPONENTS } from './components';
import { CommonModule } from '@angular/common';
import { IfloggedDirective } from './iflogged.directive';

@NgModule({
  declarations: [
    ...COMPONENTS, IfloggedDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
  ...COMPONENTS, IfloggedDirective
  ]
})
export class SharedModule { }
