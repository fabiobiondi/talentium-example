import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/internal/operators';

export interface Auth {
  token?: string;
  isAuthenticated?: boolean;
}

const INITIAL_AUTH: Auth =  {
  token: null,
  isAuthenticated: false
}

@Injectable()
export class AuthService {
  auth$: BehaviorSubject<Auth> = new BehaviorSubject<Auth>(INITIAL_AUTH)
  isAuthenticated$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) {}

  login() {
    this.http.get<Auth>(`http://localhost:3000/login`)
      .subscribe(res => {
        const auth: Auth = res;
        auth.isAuthenticated = true;
        this.auth$.next(res);

        this.isAuthenticated$.next(true);
      });
  }

  logout() {
    this.auth$.next(INITIAL_AUTH);
    this.isAuthenticated$.next(false);
  }
}















