import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'tal-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li class="nav-item"
        *ngFor="let item of items"
          (click)="tabClick.emit(item)"
      >
        <a class="nav-link"
           [ngClass]="{
            'active': item.id === active?.id
           }"
           href="#">
          {{item[labelField]}
          
        </a>
      </li>
      
    </ul>
  `
})
export class TabbarComponent {
  @Input() items;
  @Input() active;
  @Input() labelField = 'label';
  @Output() tabClick = new EventEmitter()
}


