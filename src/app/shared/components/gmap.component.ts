import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'tal-gmap',
  template: `
    <img *ngIf="lat && lng"
         src="{{urlAPI}}={{lat}},{{lng}}&zoom={{zoom}}&size={{size}}&key={{token}}" >

  `,
})
export class GmapComponent {
  @Input() lat: string;
  @Input() lng: string;
  @Input() zoom = 8;
  @Input() size = '200x200';
  urlAPI = 'https://maps.googleapis.com/maps/api/staticmap?center=';
  token = 'AIzaSyBAyMH-A99yD5fHQPz7uzqk8glNJYGEqus';
}
