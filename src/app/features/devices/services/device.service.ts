import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DeviceStore } from './device.store';
import { Device } from '../model/device';

@Injectable()
export class DeviceService {

  baseURL = 'http://localhost:3000';
  endpoint = 'devices'

  constructor(
    private http: HttpClient,
    private store: DeviceStore,
  ) {}

  getAll() {
    this.http.get<Device[]>(`${this.baseURL}/${this.endpoint}`)
      .subscribe((result: Device[]) => this.store.save(result));
  }

  setActive(device) {
    this.store.active = device;
  }


  delete(device) {
    this.http.delete(`${this.baseURL}/${this.endpoint}/${device.id}`)
      .subscribe(() => {
        this.store.delete(device.id);
      });
  }

  save(form) {
    if (this.store.active) {
      this.edit(form);
    } else {
      this.add(form);
    }
  }

  edit(form) {

    this.http.patch(`${this.baseURL}/${this.endpoint}/${this.store.active.id}`, form.value)
      .subscribe(res => {
        this.store.edit(form.value);
      });
  }

  add(form) {
    this.http.post<Device>(`${this.baseURL}/${this.endpoint}`, form.value)
      .subscribe((result: Device) => {
        this.store.add(result);
      });
  }

  reset() {
    this.store.active = null;
  }

}
